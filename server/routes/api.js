import express from 'express'
const router = express.Router()
import Api from './public'

router.use('/public', Api)

module.exports = router
