import Prismic from 'prismic-javascript'
// import PrismicDOM from 'prismic-dom'

const API_ENDPOINT = 'https://elsdance.prismic.io/api'

export default class PrismicController {

  constructor (request) {
    this.request = request
  }

  initApi (req) {
    try {
      return Prismic.getApi(API_ENDPOINT, {
        req: req
      })
    } catch (err) {
      throw err
    }
  }

  async navigation () {
    try {
      let api = await this.initApi(this.request)
      let response = await api.getSingle('navigation_bar')
      return response
    } catch (err) {
      console.log('prismic error')
      console.log(err)
      throw err
    }
  }

  async page () {
    try {
      let api = await this.initApi(this.request)
      let response = await api.getByUID('page', this.request.query.uid)
      return response
    } catch (err) {
      console.log('prismic error')
      console.log(err)
      throw err
    }
  }
}

