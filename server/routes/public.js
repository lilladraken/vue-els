import express from 'express'
let router = express.Router()
import PrismicController from './controllers/PrismicController'

router.route('/prismic/:type').all(function (req, res) {
  const prismicController = new PrismicController(req)
  prismicController[req.params.type]().then(function (prismicData) {
    res.json(prismicData)
  }).catch(function (err) {
    console.log(err)
    console.log(err.stack)
    res.status(500).send(err.message)
  })
})

module.exports = router
