// import bodyParser from 'body-parser'
// import connectHAF from 'connect-history-api-fallback'
// import connectPS from 'connect-pg-simple'
// import express from 'express'
// import expressJwt from 'express-jwt'
// import logger from 'morgan'
// import pg from 'pg'
// import session from 'express-session'
// import models from './models'
// import jwt from 'jsonwebtoken'
// import path from 'path'

// import fs from 'fs'
// import passport from 'passport'
// import saml from 'passport-saml'

// import api from './routes/api'
// import routes from './routes/index'
// import pgConfigFile from './config'

// import util from 'util'

// // modify logging options
// util.inspect.defaultOptions.depth = 2
// util.inspect.defaultOptions.colors = true

// process.on('unhandledRejection', function (err, promise) {
//   throw err
// })

// const env = process.env.NODE_ENV || 'development'
// const secretToken = 'carshare-jpadmin148150'

// // override decimal types - postgres specific oid
// pg.types.setTypeParser(1700, stringValue => {
//   return parseFloat(stringValue)
// })
// const pgConfig = pgConfigFile[env]
// const PGSession = connectPS(session)

// // default port where dev server listens for incoming traffic
// const port = process.env.PORT

// const app = express()

// let samlConfig = {
//   testing: {
//     callbackUrl: 'https://anutest.greensharecar.com.au/login/callback',
//     entryPoint: 'https://idp-dev.anu.edu.au/idp/profile/SAML2/Redirect/SSO',
//     logoutUrl: 'https://idp-dev.anu.edu.au/idp/profile/SAML2/Redirect/SSO',
//     issuer: 'https://anutest.greensharecar.com.au/shibboleth',
//     cert: 'MIIDMzCCAhugAwIBAgIUcdLjRltoRyBhcTSOBrbu+OZJT/wwDQYJKoZIhvcNAQEFBQAwHTEbMBkGA1UEAxMSaWRwLWRldi5hbnUuZWR1LmF1MB4XDTEyMDMxNTIzNTYyOVoXDTMyMDMxNTIzNTYyOVowHTEbMBkGA1UEAxMSaWRwLWRldi5hbnUuZWR1LmF1MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAldb65Y6mt934XIqL0snOhIeXk81TacTUvkTw3+J6XvA8xRyFW3LO96hys0ieH4gMwLd4reHH3THH9nwQPqu1xkMHqZUvjcG+O7iRRWuddR2aguwG3E2ufcD7TT5aPw2LC8svwtiBjsbmnim2rGHbtRYLJCu1E4OvzbimiSkUXpe6yXZCB8zBRZAJkvuuUXt74fmh7pQ+Yhc1RuhCE1qsmPoOvAOugpUK5P87wX+Tr8ECb4VTJSIJ8lvdv5L6m/dwxfU7u0tP6jUmDAWTuSZ4Dc16AH1VxN20uoTR0ExYu3585Pz7XBNdUt3BumqqXB9SQwSZ01Yi8El/d+MXkYxJSwIDAQABo2swaTBIBgNVHREEQTA/ghJpZHAtZGV2LmFudS5lZHUuYXWGKWh0dHBzOi8vaWRwLWRldi5hbnUuZWR1LmF1L2lkcC9zaGliYm9sZXRoMB0GA1UdDgQWBBTlS0U58LF54iDjkwlf5BBrsV1hMzANBgkqhkiG9w0BAQUFAAOCAQEAAKrndp6vOje1FtR5TBZyR2l2MW4rfQag7F402yQOtQPbNTmW+jrJAbpY2B4WlbMduJypJ+E1Di4SfN4eQXnX9xeaqaMRLgJUpQcsHUl0ws4VqrJ5HYtZItPZGztwj6G+GScdhhc6+Cf6K75cckIpKFU+gILFmOPWVd3LIYWBqFxTw2Q/CbCLq2ZnKPkXaaHh4gxajV/lIMS4oDeEAFIUjzGjEu/BFPTS944SmBrtnWC1xrXRZBUnexqB6mVdQIKTsJxovEQWQsu+ruC7htNMNQzbK7SLiWrn2gVlIwV2izFi15q8vlXGi9ClZPDt2VcJae8YGt7UMeQu3MfPrdYNZQ==',
//     decryptionPvk: fs.readFileSync('./private-dev.pem', 'utf-8'),
//     signatureAlgorithm: 'sha256',
//     passReqToCallbackUrl: false
//   },
//   development: {
//     callbackUrl: 'https://anutest.greensharecar.com.au/login/callback',
//     entryPoint: 'https://idp-dev.anu.edu.au/idp/profile/SAML2/Redirect/SSO',
//     logoutUrl: 'https://idp-dev.anu.edu.au/idp/profile/SAML2/Redirect/SSO',
//     issuer: 'https://anutest.greensharecar.com.au/shibboleth',
//     cert: 'MIIDMzCCAhugAwIBAgIUcdLjRltoRyBhcTSOBrbu+OZJT/wwDQYJKoZIhvcNAQEFBQAwHTEbMBkGA1UEAxMSaWRwLWRldi5hbnUuZWR1LmF1MB4XDTEyMDMxNTIzNTYyOVoXDTMyMDMxNTIzNTYyOVowHTEbMBkGA1UEAxMSaWRwLWRldi5hbnUuZWR1LmF1MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAldb65Y6mt934XIqL0snOhIeXk81TacTUvkTw3+J6XvA8xRyFW3LO96hys0ieH4gMwLd4reHH3THH9nwQPqu1xkMHqZUvjcG+O7iRRWuddR2aguwG3E2ufcD7TT5aPw2LC8svwtiBjsbmnim2rGHbtRYLJCu1E4OvzbimiSkUXpe6yXZCB8zBRZAJkvuuUXt74fmh7pQ+Yhc1RuhCE1qsmPoOvAOugpUK5P87wX+Tr8ECb4VTJSIJ8lvdv5L6m/dwxfU7u0tP6jUmDAWTuSZ4Dc16AH1VxN20uoTR0ExYu3585Pz7XBNdUt3BumqqXB9SQwSZ01Yi8El/d+MXkYxJSwIDAQABo2swaTBIBgNVHREEQTA/ghJpZHAtZGV2LmFudS5lZHUuYXWGKWh0dHBzOi8vaWRwLWRldi5hbnUuZWR1LmF1L2lkcC9zaGliYm9sZXRoMB0GA1UdDgQWBBTlS0U58LF54iDjkwlf5BBrsV1hMzANBgkqhkiG9w0BAQUFAAOCAQEAAKrndp6vOje1FtR5TBZyR2l2MW4rfQag7F402yQOtQPbNTmW+jrJAbpY2B4WlbMduJypJ+E1Di4SfN4eQXnX9xeaqaMRLgJUpQcsHUl0ws4VqrJ5HYtZItPZGztwj6G+GScdhhc6+Cf6K75cckIpKFU+gILFmOPWVd3LIYWBqFxTw2Q/CbCLq2ZnKPkXaaHh4gxajV/lIMS4oDeEAFIUjzGjEu/BFPTS944SmBrtnWC1xrXRZBUnexqB6mVdQIKTsJxovEQWQsu+ruC7htNMNQzbK7SLiWrn2gVlIwV2izFi15q8vlXGi9ClZPDt2VcJae8YGt7UMeQu3MfPrdYNZQ==',
//     decryptionPvk: fs.readFileSync('./private-dev.pem', 'utf-8'),
//     signatureAlgorithm: 'sha256',
//     passReqToCallbackUrl: false
//   },
//   production: {
//     callbackUrl: 'https://anutest.greensharecar.com.au/login/callback',
//     entryPoint: 'https://idp-dev.anu.edu.au/idp/profile/SAML2/Redirect/SSO',
//     logoutUrl: 'https://idp-dev.anu.edu.au/idp/profile/SAML2/Redirect/SSO',
//     issuer: 'https://anutest.greensharecar.com.au/shibboleth',
//     cert: 'MIIDMzCCAhugAwIBAgIUcdLjRltoRyBhcTSOBrbu+OZJT/wwDQYJKoZIhvcNAQEFBQAwHTEbMBkGA1UEAxMSaWRwLWRldi5hbnUuZWR1LmF1MB4XDTEyMDMxNTIzNTYyOVoXDTMyMDMxNTIzNTYyOVowHTEbMBkGA1UEAxMSaWRwLWRldi5hbnUuZWR1LmF1MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAldb65Y6mt934XIqL0snOhIeXk81TacTUvkTw3+J6XvA8xRyFW3LO96hys0ieH4gMwLd4reHH3THH9nwQPqu1xkMHqZUvjcG+O7iRRWuddR2aguwG3E2ufcD7TT5aPw2LC8svwtiBjsbmnim2rGHbtRYLJCu1E4OvzbimiSkUXpe6yXZCB8zBRZAJkvuuUXt74fmh7pQ+Yhc1RuhCE1qsmPoOvAOugpUK5P87wX+Tr8ECb4VTJSIJ8lvdv5L6m/dwxfU7u0tP6jUmDAWTuSZ4Dc16AH1VxN20uoTR0ExYu3585Pz7XBNdUt3BumqqXB9SQwSZ01Yi8El/d+MXkYxJSwIDAQABo2swaTBIBgNVHREEQTA/ghJpZHAtZGV2LmFudS5lZHUuYXWGKWh0dHBzOi8vaWRwLWRldi5hbnUuZWR1LmF1L2lkcC9zaGliYm9sZXRoMB0GA1UdDgQWBBTlS0U58LF54iDjkwlf5BBrsV1hMzANBgkqhkiG9w0BAQUFAAOCAQEAAKrndp6vOje1FtR5TBZyR2l2MW4rfQag7F402yQOtQPbNTmW+jrJAbpY2B4WlbMduJypJ+E1Di4SfN4eQXnX9xeaqaMRLgJUpQcsHUl0ws4VqrJ5HYtZItPZGztwj6G+GScdhhc6+Cf6K75cckIpKFU+gILFmOPWVd3LIYWBqFxTw2Q/CbCLq2ZnKPkXaaHh4gxajV/lIMS4oDeEAFIUjzGjEu/BFPTS944SmBrtnWC1xrXRZBUnexqB6mVdQIKTsJxovEQWQsu+ruC7htNMNQzbK7SLiWrn2gVlIwV2izFi15q8vlXGi9ClZPDt2VcJae8YGt7UMeQu3MfPrdYNZQ==',
//     decryptionPvk: fs.readFileSync('./private-dev.pem', 'utf-8'),
//     signatureAlgorithm: 'sha256',
//     passReqToCallbackUrl: false
//   }
// }
// var samlStrategy = new saml.Strategy(samlConfig[env], function (profile, done) {
//   let user = {
//     sessionIndex: profile.sessionIndex,
//     uid: profile['urn:oid:0.9.2342.19200300.100.1.1'],
//     eduPersonAffiliation: profile['urn:oid:1.3.6.1.4.1.5923.1.1.1.1'],
//     surname: profile['urn:oid:2.5.4.4'],
//     personalTitle: profile['urn:oid:0.9.2342.19200300.100.1.40'],
//     email: profile['urn:oid:0.9.2342.19200300.100.1.3'],
//     displayName: profile['urn:oid:2.16.840.1.113730.3.1.241'],
//     affiliations: profile['urn:oid:2.16.840.1.113730.3.1.2']
//   }
//   return done(null, user)
// })

// samlStrategy._saml.options.identifierFormat = null
// passport.serializeUser(function (user, done) {
//   done(null, user)
// })

// passport.deserializeUser(function (user, done) {
//   done(null, user)
// })
// passport.use(samlStrategy)

// app.use(function (req, res, next) {
//   let referrers = req.get('X-Forwarded-For').split(',')
//   if (referrers.length === 1) {
//     let token = req.originalUrl.split('/').pop()
//     res.set('Content-Type', 'text/plain')
//     res.send(`${token}.r_7Bgyh17AiwnI3TeugxStlR2Ma39fWnmPgVM6JhM6c`)
//     res.end()
//   } else {
//     next()
//   }
// })

// app.use('/login/saml', passport.authenticate('saml', {
//   successRedirect: '/login/callback',
//   failureRedirect: '/',
//   failureFlash: true
// }))
// app.use('/login/callback',
//   passport.authenticate('saml', { failureRedirect: '/', failureFlash: true }), function (req, res) {
//     console.log('attempting to find user')
//     models.Account.findOne({
//       where: { username: { $iLike: req.session.passport.user.uid } },
//       include: [
//         models.AccountType,
//         { model: models.Organization, where: { name: { $contains: [`${req.hostname}`] } } }
//       ]
//     }).then((userModel) => {
//       if (userModel) {
//         let user = userModel.toJSON()
//         if (user.card_id === '0000000' && user.activated) {
//           res.redirect('/auth/unscanned-card')
//         } else if (user.activated) {
//           let token = jwt.sign(user, 'carshare-jpadmin148150', { expiresIn: 60 * 60 * 5 })
//           req.session.token = token
//           res.redirect('/dashboard')
//         } else {
//           res.redirect('/auth/unactivated-account')
//         }
//       } else {
//         let token = jwt.sign(req.session.passport.user, 'carshare-jpadmin148150', { expiresIn: 60 * 60 * 5 })
//         req.session.token = token
//         res.redirect('/register')
//       }
//     })
//   }
// )

// app.get('/logout', function (req, res) {
//   req.session.destroy(function (err) {
//     if (err) console.log(err)
//     res.redirect('/loggedout')
//   })
// })

// app.post('/logout', function (req, res) {
//   req.session.destroy(function (err) {
//     if (err) console.log(err)
//     req.session = null
//     res.json({ loggedOut: true })
//   })
// })

// app.get('/Shibboleth.sso/Metadata', function (req, res, next) {
//   let decryptionCert = 'MIIGETCCBPmgAwIBAgISAUtKP153+rLcg0+zEcABPJiQMA0GCSqGSIb3DQEBCwUAMEoxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MSMwIQYDVQQDExpMZXQncyBFbmNyeXB0IEF1dGhvcml0eSBYMTAeFw0xNjAyMDkyMzEwMDBaFw0xNjA1MDkyMzEwMDBaMCQxIjAgBgNVBAMTGXd3dy5yZXNlcnZlLnJvc2Nvbi5jb20uYXUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDaw/SDnwukJavHo+TMWcsbjwUdOJcrnIYFfYIosRpbTBOPLhVeI9UkQebDaNPa4j4C+4d/UR1AJAX7IvtemayIScME72YULYA2v45eQ5JyT3hG7aK60boy6svMH2A9M1i59qvB3Aq356TNQZHEd9ZoLZXO9b2iInyNPdqr8PnoXZGczu/P/gRzZDVKJwKGmX0hwGo7bWrqCHMd+gmgQYrbYQQWkxy9PzXUYOhGQxYJFsgHFJiYsae8BW/szpuX6NF0mK+HeAi5kGsPehrsYpoDGZ/uGDMOei3ISInDl+PPIJC6AVeELb2LJ4qg1OeYzoKpM9pzfrvhUYRsmA3E+m1zAgMBAAGjggMVMIIDETAOBgNVHQ8BAf8EBAMCBaAwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCMAwGA1UdEwEB/wQCMAAwHQYDVR0OBBYEFGNMkedaB+20DrOKywjolmHqO/ntMB8GA1UdIwQYMBaAFKhKamMEfd265tE5t6ZFZe/zqOyhMHAGCCsGAQUFBwEBBGQwYjAvBggrBgEFBQcwAYYjaHR0cDovL29jc3AuaW50LXgxLmxldHNlbmNyeXB0Lm9yZy8wLwYIKwYBBQUHMAKGI2h0dHA6Ly9jZXJ0LmludC14MS5sZXRzZW5jcnlwdC5vcmcvMIIBHQYDVR0RBIIBFDCCARCCGnJlc2VydmUuYm9keWNvcnBvbmxpbmUuY29tghl3d3cucmVzZXJ2ZS5yb3Njb24uY29tLmF1ghVyZXNlcnZlLnJvc2Nvbi5jb20uYXWCFnd3dy5yZXNlcnZlLnJvc2Nvbi5jb22CGnd3dy5yZXNlcnZlLnRlY2hjYXIuY29tLmF1gh1yZXNlcnZlLmJvZHljb3Jwb25saW5lLmNvbS5hdYIScmVzZXJ2ZS5yb3Njb24uY29tghZyZXNlcnZlLnRlY2hjYXIuY29tLmF1gh53d3cucmVzZXJ2ZS5ib2R5Y29ycG9ubGluZS5jb22CIXd3dy5yZXNlcnZlLmJvZHljb3Jwb25saW5lLmNvbS5hdTCB/gYDVR0gBIH2MIHzMAgGBmeBDAECATCB5gYLKwYBBAGC3xMBAQEwgdYwJgYIKwYBBQUHAgEWGmh0dHA6Ly9jcHMubGV0c2VuY3J5cHQub3JnMIGrBggrBgEFBQcCAjCBngyBm1RoaXMgQ2VydGlmaWNhdGUgbWF5IG9ubHkgYmUgcmVsaWVkIHVwb24gYnkgUmVseWluZyBQYXJ0aWVzIGFuZCBvbmx5IGluIGFjY29yZGFuY2Ugd2l0aCB0aGUgQ2VydGlmaWNhdGUgUG9saWN5IGZvdW5kIGF0IGh0dHBzOi8vbGV0c2VuY3J5cHQub3JnL3JlcG9zaXRvcnkvMA0GCSqGSIb3DQEBCwUAA4IBAQAoPZ/qqxNpKYVVZUlUscWYR5JLGtzG3denVMiFmzM0QhZmduFK24nCRhRnFx/qs3UVsLmP+TW4TzuVFJnSlW4x47j3/3oJdFe+1JgE48a5bXDkg7XCyRdfd2slAEN5wkPsBZ6QUqsB+le0vRYauGtntGcqp/tyCwlWI/QrBmotIbuxVOh16ZpK+XGTabkN4Lrf7HcLQlqTfG5oaQSjWr7B9MA8LC+Z6DrbbGXmDxuILM6v+Y3lQBJ9thcxbA2GwO/OUEUloRdB4N0FqPAc2BqGW9M4WweM1n6tcdf8UlnBEhZmj4kHWaSaDvlHfLnZJmom8VF7Q81tvZo4Utl6rHFO'
//   let metadata = samlStrategy.generateServiceProviderMetadata(decryptionCert)
//   res.type('application/xml')
//   res.send(metadata)
// })

// // handle fallback for HTML5 history API
// app.use(connectHAF())

// // serve pure static assets
// const staticPath = path.posix.join('/', '')
// const staticAssets = express.static('./dist')
// app.use(staticPath, function (req, res, next) {
//   if (req.url === '/index.html') return next()
//   else return staticAssets(req, res, next)
// })

// // enable bodyparser
// app.use(bodyParser.json({ limit: '50mb' }))
// app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }))

// // set up pg session store
// const conString = `postgres://${pgConfig.username}:${pgConfig.password}@${pgConfig.host}:5432/${pgConfig.database}`
// const tableName = 'Sessions'
// app.use(session({
//   store: new PGSession({
//     pg,
//     conString,
//     tableName
//   }),
//   secret: secretToken,
//   resave: false,
//   cookie: { maxAge: 30 * 24 * 60 * 60 * 1000 },
//   saveUninitialized: false
// }))

// // set up logger
// app.use(logger(':date[web] - [:method] - :url - :status'))

// // disable cache for XHR requests
// app.use(function (req, res, next) {
//   if (req.xhr) res.set('Expires', '-1')
//   next()
// })

// // authorisation for XHR requests
// app.use('/api/private', expressJwt({
//   secret: secretToken,
//   getToken: function fromHeaderOrQuerystring (req) {
//     if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
//       return req.headers.authorization.split(' ')[1]
//     } else if (req.session.token) {
//       return req.session.token
//     } else if (req.query && req.query.token) {
//       return req.query.token
//     } else if (req.body && req.body.token) {
//       return req.body.token
//     }
//     return null
//   }
// }))

// app.use('/api/public', expressJwt({
//   secret: secretToken,
//   credentialsRequired: false,
//   getToken: function fromHeaderOrQuerystring (req) {
//     if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
//       return req.headers.authorization.split(' ')[1]
//     } else if (req.session.token) {
//       return req.session.token
//     } else if (req.query && req.query.token) {
//       return req.query.token
//     } else if (req.body && req.body.token) {
//       return req.body.token
//     }
//     return false
//   }
// }))

// // app.set('view engine', 'hbs')

// // set up routes and api
// app.use(function (err, req, res, next) {
//   if (err.name === 'UnauthorizedError') {
//     req.session.destroy()
//   }
//   next()
// })
// app.use('/', routes)
// app.use('/api', api)

// // catch 404 and forward to error handler
// app.use(function (req, res, next) {
//   fs.readFile(path.join(__dirname, '../dist/index.html'), 'utf-8', function (err, data) {
//     if (err) console.log(err)
//     let homePage = data
//     if (req.query.token) {
//       let html = `<script type='text/javascript'>
//       window.sessionStorage.setItem('token', '${req.query.token}')
//       </script>`
//       homePage = homePage.replace('</head>', `${html}</head>`)
//     }
//     res.set('Content-Type', 'text/html')
//     res.send(homePage)
//     res.end()
//   })
// })

// const server = require('http').createServer(app)
// server.listen(port, function (err) {
//   if (err) {
//     console.log(err)
//     return
//   }
// })

// module.exports = {
//   close: () => {
//     server.close()
//   }
// }
