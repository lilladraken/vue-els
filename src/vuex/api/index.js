import axios from 'axios'

let instance = axios.create({
  baseURL: '/api',
  timeout: 15000
})

instance.interceptors.request.use(function (config) {
  let loadingDiv = document.createElement('div')
  let spinnerDiv = document.createElement('div')
  loadingDiv.id = 'loadingDiv'
  spinnerDiv.className = 'spinner-div'
  loadingDiv.className = 'loading'
  let exists = document.getElementById('loadingDiv')
  if (!exists) {
    document.body.appendChild(loadingDiv)
    loadingDiv.appendChild(spinnerDiv)
    loadingDiv.setAttribute('data-requests', 1)
    loadingDiv.setAttribute('data-responses', 0)
  } else {
    let count = parseInt(exists.getAttribute('data-requests'))
    exists.setAttribute('data-requests', count + 1)
  }
  return config
}, function (error) {
  return Promise.reject(error.response)
})

instance.interceptors.response.use(function (response) {
  let loadingDivRemove = document.getElementById('loadingDiv')
  if (loadingDivRemove) {
    let count = parseInt(loadingDivRemove.getAttribute('data-responses'))
    loadingDivRemove.setAttribute('data-responses', count + 1)
    let requests = parseInt(loadingDivRemove.getAttribute('data-requests'))
    let responses = parseInt(loadingDivRemove.getAttribute('data-responses'))
    if (requests === responses) {
      loadingDivRemove.parentNode.removeChild(loadingDivRemove)
    }
  }
  return response
}, function (error) {
  let loadingDivRemove = document.getElementById('loadingDiv')
  if (loadingDivRemove) {
    let count = parseInt(loadingDivRemove.getAttribute('data-responses'))
    loadingDivRemove.setAttribute('data-responses', count + 1)
    let requests = parseInt(loadingDivRemove.getAttribute('data-requests'))
    let responses = parseInt(loadingDivRemove.getAttribute('data-responses'))
    if (requests === responses) {
      loadingDivRemove.parentNode.removeChild(loadingDivRemove)
    }
  }
  if (error.response && error.response.status === 401) {
    return Promise.reject(error.response)
  }
})

class Api {

  async getNavigation () {
    let { data } = await instance.get('public/prismic/navigation')
    return data
  }

  async getPage (uid) {
    let { data } = await instance.get('public/prismic/page', {
      params: { uid }
    })
    return data
  }

}

export default Api
